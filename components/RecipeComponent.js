import classNames from 'classnames'
import { confirmAlert } from 'react-confirm-alert'
import 'react-confirm-alert/src/react-confirm-alert.css'
import ClipLoader from "react-spinners/ClipLoader";
import { useState } from 'react'
import useLocalStorage from '../hooks/useLocalStorage'

const Recipe = ({ recipe, className, canStop = false }) => {
  const [loading, setLoading] = useState(false)
  const [currentWorkspace] = useLocalStorage('workspace')

  const startRecipe = () => {
    confirmAlert({
      title: 'Confirm starting recipe',
      message: `Are you sure you want to start recipe: '${recipe.name}'?`,
      buttons: [
        {
          label: 'Yes',
          onClick: async () => {
            setLoading(true)
            const res = await fetch(`/api/recipe/start`, {
              method: 'PUT',
              headers: {
                'Content-Type': 'application/json',
                'x-workspace': currentWorkspace
              },
              body: JSON.stringify({
                id: recipe.id
              })
            })

            const data = await res.json()

            setLoading(false)
            if (data.success && process.browser) {
              window.location.reload()
            }
          }
        },
        {
          label: 'No',
        }
      ]
    })
  }

  const stopRecipe = async () => {
    confirmAlert({
      title: 'Confirm stopping recipe',
      message: `Are you sure you want to stop recipe: '${recipe.name}'?`,
      buttons: [
        {
          label: 'Yes',
          onClick: async () => {
            setLoading(true)
            const res = await fetch(`/api/recipe/stop`, {
              method: 'PUT',
              headers: {
                'Content-Type': 'application/json',
                'x-workspace': currentWorkspace
              },
              body: JSON.stringify({
                id: recipe.id
              })
            })

            const data = await res.json()

            setLoading(false)
            if (data.success && process.browser) {
              window.location.reload()
            }
          }
        },
        {
          label: 'No',
        }
      ]
    })
  }

  return (
    <div className={classNames(className, 'rounded border dark:border-gray-800 border-gray-400 dark:bg-gray-900 bg-white relative')}>
      {
        recipe.running ?
          <div className="absolute w-full h-2 top-0 bg-green-500 rounded-t-sm"></div>
        :
          <div className="absolute w-full h-2 top-0 bg-red-500 rounded-t-sm"></div>
      }
      <div className="flex flex-col p-4 h-full">
        <div className="my-2 flex flex-row">
          <span className="dark:text-gray-200 text-gray-700">{recipe.name}</span>
          <span className="text-gray-400 text-sm ml-auto">#{recipe.id}</span>
        </div>
        <hr className="mb-2"></hr>
        {/* Actions */}
        <div className="justify-self-end flex flex-row items-center mt-auto ml-auto">
          <a href={`https://www.workato.com/recipes/${recipe.id}#jobs`} target="_blank" className="inline-flex items-center px-2.5 py-1.5 border border-transparent text-xs font-medium rounded text-indigo-700 bg-indigo-100 hover:bg-indigo-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500" title="Open Jobs in workato">
            Jobs
          </a>
          <a href={`https://www.workato.com/recipes/${recipe.id}#recipe`} className="inline-flex items-center px-2.5 py-1.5 border border-transparent text-xs font-medium rounded text-indigo-700 bg-indigo-100 hover:bg-indigo-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 ml-2" title="Open Recipe in Workato" target="_blank">
            Recipe
          </a>
          {
            !recipe.running && 
            <button href={`https://www.workato.com/recipes/${recipe.id}#recipe`} className="ml-2 inline-flex items-center px-2.5 py-1.5 border border-transparent text-xs font-medium rounded text-green-700 bg-green-100 hover:bg-green-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-500" onClick={startRecipe} disabled={loading}>
              {
                !loading && 'Start Recipe'
              }
              {
                loading && <div className="flex flex-row items-center">
                  <span className="mr-2">Starting Recipe</span>
                  <ClipLoader color="#047857" size="15"/>
                </div>
              }
            </button>
          }
          {
            recipe.running && canStop &&
            <button href={`https://www.workato.com/recipes/${recipe.id}#recipe`} className="ml-2 inline-flex items-center px-2.5 py-1.5 border border-transparent text-xs font-medium rounded text-red-700 bg-red-100 hover:bg-red-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-500" onClick={stopRecipe} disabled={loading}>
              {
                !loading && 'Stop Recipe'
              }
              {
                loading && <div className="flex flex-row items-center">
                  <span className="mr-2">Stopping Recipe</span>
                  <ClipLoader color="#b91c1c" size="15"/>
                </div>
              }
            </button>
          }
        </div>
      </div>
    </div>
  )
}

export default Recipe