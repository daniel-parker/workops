import { useTimestamp } from "../../hooks/useTimestamp"
import classNames from 'classnames'
import { useEffect, useState } from "react"
import { logEntryRow, yellow, red, blue } from './LogsListComponent.module.scss'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faExclamationCircle, faExclamationTriangle, faExternalLink, faInfoSquare } from "@fortawesome/pro-duotone-svg-icons"

const LogEntryComponent = ({ logEntry, recipeNamesById }) => {
  const timestampString = useTimestamp(logEntry.timestamp.seconds)

  const getRowBackgroundColor = () => {
    switch (logEntry.severity) {
      case 'WARNING':
        return `bg-yellow-50 hover:bg-yellow-100 ${yellow}`
      case 'ERROR':
      case 'CRITICAL':
      case 'ALERT':
      case 'EMERGENCY':
        return `bg-red-50 hover:bg-red-100 ${red}`
      default:
        return `hover:bg-blue-50 ${blue}`
    }
  }

  const getSeverityIcon = () => {
    switch(logEntry.severity) {
      case 'WARNING':
        return faExclamationTriangle
      case 'ERROR':
      case 'CRITICAL':
      case 'ALERT':
      case 'EMERGENCY':
        return faExclamationCircle
      default:
        return faInfoSquare
    }
  }

  const getSeverityIconColor = () => {
    switch(logEntry.severity) {
      case 'WARNING':
        return 'text-yellow-500'
      case 'ERROR':
      case 'CRITICAL':
      case 'ALERT':
      case 'EMERGENCY':
        return 'text-red-500'
      default:
        return 'text-blue-500'
    }
  }

  return (
    <tr className={classNames("border", getRowBackgroundColor(), logEntryRow)}>
      <td className="px-4 py-1"><FontAwesomeIcon icon={getSeverityIcon()} fixedWidth className={classNames(getSeverityIconColor())}/> {logEntry.severity}</td>
      <td className="px-4 py-1">{timestampString}</td>
      <td className="px-4 py-1">{logEntry.jsonPayload.fields.logmessage.stringValue}</td>
      <td className="px-4 py-1">
        <a href={`https://www.workato.com/recipes/${logEntry.labels.recipeId}/job/${logEntry.labels.jobId}`} target="_blank" className="inline-flex items-center border border-transparent text-xs font-medium rounded text-indigo-700 hover:text-indigo-400" title="Open job in Workato">
          {logEntry.labels.jobId}
        </a>
      </td>
      <td className="px-4 py-1">
        <a href={`https://www.workato.com/recipes/${logEntry.labels.recipeId}`} target="_blank" className="inline-flex items-center border border-transparent text-xs font-medium rounded text-indigo-700 hover:text-indigo-400" title="Open recipe in Workato">
          {recipeNamesById[logEntry.labels.recipeId]}
        </a>
      </td>
    </tr>
  )
}

export const LogsListComponent = ({ className, logEntries, recipes }) => {
  const [recipeNamesById, setRecipeNamesById] = useState({})

  useEffect(() => {
    const lRecipeNamesById = {}

    recipes.map(r => {
      lRecipeNamesById[r.id] = r.name
    })

    setRecipeNamesById(lRecipeNamesById)
  }, [recipes])

  return (
    <table className={classNames("table-auto border-collapse bg-white border text-gray-800 text-sm relative", className)}>
      <thead>
        <tr className="text-left border">
          <th className="border border-gray-200 bg-gray-200 px-4 py-1 font-semibold sticky top-0">Severity</th>
          <th className="border border-gray-200 bg-gray-200 px-4 py-1 font-semibold sticky top-0">Timestamp</th>
          <th className="border border-gray-200 bg-gray-200 px-4 py-1 font-semibold sticky top-0">Log message</th>
          <th className="border border-gray-200 bg-gray-200 px-4 py-1 font-semibold sticky top-0">Job ID <FontAwesomeIcon icon={faExternalLink}/></th>
          <th className="border border-gray-200 bg-gray-200 px-4 py-1 font-semibold sticky top-0">Recipe <FontAwesomeIcon icon={faExternalLink}/></th>
        </tr>
      </thead>
      <tbody className="text-left">
      {
        logEntries && logEntries.map((logEntry, i) => (
          <LogEntryComponent key={i} logEntry={logEntry} recipeNamesById={recipeNamesById}/>
        ))
      }
      </tbody>
    </table>
  )
}
