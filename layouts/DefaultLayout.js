import { Transition } from "@tailwindui/react"
import Link from "next/link"
import { useEffect, useState } from "react"
import useLocalStorage from "../hooks/useLocalStorage"
import classNames from 'classnames'
import { useSession, signOut } from 'next-auth/client'
import useWorkatoWorkspaces from "../hooks/useWorkatoWorkspaces"

const DefaultLayout = ({ children }) => {
  const workatoWorkspaces = useWorkatoWorkspaces()
  const [session, loading] = useSession()
  const [firstLoad, setFirstLoad] = useState(true)
  const [showProfileDropdown, setShowProfileDropdown] = useState(false)
  const [showWorkspaceSelector, setShowWorkspaceSelector] = useState(false)
  const [theme, setTheme] = useLocalStorage('theme', null)
  const [currentWorkspace, setCurrentWorkspace] = useLocalStorage('workspace')
  const [profileImageUrl, setProfileImageUrl] = useState(null)
  const [menuCollapsed, setMenuCollapsed] = useState(true)

  useEffect(() => {
    if (!firstLoad && session) {
      window.location.pathname = '/'
    }
  }, [currentWorkspace])

  useEffect(() => {
    if (session) {
      setFirstLoad(false)
    }
  }, [session])

  useEffect(() => {
    if (session) {
      (async function() {
        const res = await fetch(`/api/profile/photo/${encodeURIComponent(session.user.email)}`, {
          method: 'GET',
        })
  
        const data = await res.json()
  
        setProfileImageUrl(data.url)
      })()
    }
  }, [session])

  if (process.browser) {
    if (theme === 'dark' || (!theme && window.matchMedia('(prefers-color-scheme: dark)').matches)) {
      document.querySelector('html').classList.add('dark')
    } else {
      document.querySelector('html').classList.remove('dark')
    }
  }
  
  const toggleShowProfileDropdown = () => {
    setShowProfileDropdown(!showProfileDropdown)
  }

  const enableDarkMode = () => {
    setTheme('dark')
  }

  const enableLightMode = () => {
    setTheme('light')
  }

  const redirectToLogin = () => {
    if (process.browser) {
      window.location.pathname = '/login'
    }
  }

  return (
    <div className="h-screen flex overflow-hidden dark:bg-black bg-gray-100">
      {
        !session && !loading && redirectToLogin()
      }
      { session && <>
        {/* Off-canvas menu for mobile, show/hide based on off-canvas menu state. */}
        <div className={classNames(menuCollapsed ? 'hidden' : '', 'md:hidden')}>
          <div className="fixed inset-0 flex z-40">
            <Transition
              show={!menuCollapsed}
              enter="transition-opacity ease-linear duration-300"
              enterFrom="opacity-0"
              enterTo="opacity-100"
              leave="transition-opacity ease-linear duration-300"
              leaveFrom="opacity-100"
              leaveTo="opacity-0">
              <div className="fixed inset-0" aria-hidden="true">
                <div className="absolute inset-0 bg-gray-600 opacity-75"></div>
              </div>
            </Transition>
            
            <Transition
              show={!menuCollapsed}
              enter="transition ease-in-out duration-300 transform"
              enterFrom="-translate-x-full"
              enterTo="translate-x-0"
              leave="transition ease-in-out duration-300 transform"
              leaveFrom="translate-x-0"
              leaveTo="-translate-x-full">
              <div className="relative flex-1 flex flex-col max-w-xs w-full h-full pt-5 pb-4 bg-gray-800">
                <div className="absolute top-0 right-0 -mr-12 pt-2">
                  <button className="ml-1 flex items-center justify-center h-10 w-10 rounded-full focus:outline-none focus:ring-2 focus:ring-inset focus:ring-white" onClick={() => setMenuCollapsed(true)}>
                    <span className="sr-only">Close sidebar</span>
                    {/* Heroicon name: x */}
                    <svg className="h-6 w-6 text-white" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                      <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M6 18L18 6M6 6l12 12" />
                    </svg>
                  </button>
                </div>
                <div className="flex-shrink-0 flex items-center px-4">
                  <h1 className="text-white text-2xl font-semibold">workops</h1>
                </div>
                <div className="mt-5 flex-1 h-0 overflow-y-auto">
                  <nav className="px-2 space-y-1">
                    {/* Current: "bg-gray-900 text-white", Default: "text-gray-300 hover:bg-gray-700 hover:text-white" */}
                    <Link href="/">
                      {/* Current: "text-gray-300", Default: "text-gray-400 group-hover:text-gray-300" */}
                      {/* Heroicon name: collection */}
                      <span className="bg-gray-900 text-white group flex items-center px-2 py-2 text-base font-medium rounded-md cursor-pointer">
                        <svg className="w-6 h-6 mr-2" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M19 11H5m14 0a2 2 0 012 2v6a2 2 0 01-2 2H5a2 2 0 01-2-2v-6a2 2 0 012-2m14 0V9a2 2 0 00-2-2M5 11V9a2 2 0 012-2m0 0V5a2 2 0 012-2h6a2 2 0 012 2v2M7 7h10" /></svg>
                        Integrations
                      </span>
                    </Link>
                  </nav>
                </div>
              </div>
            </Transition>
            
            <div className="flex-shrink-0 w-14" aria-hidden="true">
              {/* Dummy element to force sidebar to shrink to fit close icon */}
            </div>
          </div>
        </div>

        {/* Static sidebar for desktop */}
        <div className="hidden md:flex md:flex-shrink-0">
          <div className="flex flex-col w-64">
            {/* Sidebar component, swap this element with another sidebar if you like */}
            <div className="flex flex-col h-0 flex-1">
              <div className="flex items-center h-16 flex-shrink-0 px-4 bg-gray-900">
                <h1 className="text-white text-2xl font-semibold">workops</h1>
              </div>
              <div className="flex-1 flex flex-col overflow-y-auto">
                <nav className="flex-1 px-2 py-4 bg-gray-800 space-y-1">
                  {/* Current: "bg-gray-200 text-gray-900", Default: "text-gray-600 hover:bg-gray-50 hover:text-gray-900" */}
                  <Link href="/">
                    {/* Current: "text-gray-300", Default: "text-gray-400 group-hover:text-gray-300" */}
                    {/* Heroicon name: collection */}
                    <span className="bg-gray-900 text-white group flex items-center px-2 py-2 text-sm font-medium rounded-md cursor-pointer">
                      <svg className="w-6 h-6 mr-2" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M19 11H5m14 0a2 2 0 012 2v6a2 2 0 01-2 2H5a2 2 0 01-2-2v-6a2 2 0 012-2m14 0V9a2 2 0 00-2-2M5 11V9a2 2 0 012-2m0 0V5a2 2 0 012-2h6a2 2 0 012 2v2M7 7h10" /></svg>
                      Integrations
                    </span>
                  </Link>
                </nav>
              </div>
            </div>
          </div>
        </div>
        <div className="flex flex-col w-0 flex-1 overflow-hidden">
          <div className="relative z-10 flex-shrink-0 flex h-16 dark:bg-black bg-white shadow">
            <button className="px-4 border-r border-gray-200 text-gray-500 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500 md:hidden" onClick={() => setMenuCollapsed(false)}>
              <span className="sr-only">Open sidebar</span>
              {/* Heroicon name: menu-alt-2 */}
              <svg className="h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M4 6h16M4 12h16M4 18h7" />
              </svg>
            </button>
            <div className="flex-1 px-4 flex justify-between items-center">
              <div className="ml-auto mr-4">
                <div className="relative inline-block text-left">
                  <div>
                    <button type="button" onClick={() => setShowWorkspaceSelector(!showWorkspaceSelector)} className="inline-flex justify-center w-full rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-100 focus:ring-indigo-500" id="options-menu" aria-haspopup="true" aria-expanded="true">
                      {
                        currentWorkspace && currentWorkspace
                      }
                      {
                        !currentWorkspace && 'Select Workspace'
                      }
                      <svg className="-mr-1 ml-2 h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                        <path fillRule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clipRule="evenodd" />
                      </svg>
                    </button>
                  </div>

                  <Transition
                    show={showWorkspaceSelector}
                    enter="transition ease-out duration-100"
                    enterFrom="transform opacity-0 scale-95"
                    enterTo="transform opacity-100 scale-100"
                    leaveFrom="transform opacity-100 scale-100"
                    leaveTo="transform opacity-0 scale-95">
                    <div className="origin-top-right absolute right-0 mt-2 w-56 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5">
                      <div className="py-1" role="menu" aria-orientation="vertical" aria-labelledby="options-menu">
                        {
                          workatoWorkspaces &&
                          Object.keys(workatoWorkspaces).map(workatoWorkspace => (
                            <a key={workatoWorkspace} onClick={() => {setCurrentWorkspace(workatoWorkspace); setShowWorkspaceSelector(false)}} className="cursor-pointer block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 hover:text-gray-900" role="menuitem">{workatoWorkspace}</a>
                          ))
                        }
                      </div>
                    </div>
                  </Transition>
                </div>
              </div>
              <div className="flex items-center">
                {
                  theme === 'dark' ? 
                  <button className="dark:bg-black bg-white p-1 rounded-full text-gray-400 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500" onClick={enableLightMode}>  
                    <span className="sr-only">Toggle Dark Mode</span>
                    {/* Heroicon name: Sun */}
                    <svg className="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M12 3v1m0 16v1m9-9h-1M4 12H3m15.364 6.364l-.707-.707M6.343 6.343l-.707-.707m12.728 0l-.707.707M6.343 17.657l-.707.707M16 12a4 4 0 11-8 0 4 4 0 018 0z" /></svg>
                  </button>
                  :
                  <button className="dark:bg-black bg-white p-1 rounded-full text-gray-400 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500" onClick={enableDarkMode}>  
                    <span className="sr-only">Toggle Dark Mode</span>
                    {/* Heroicon name: Moon */}
                    <svg className="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M20.354 15.354A9 9 0 018.646 3.646 9.003 9.003 0 0012 21a9.003 9.003 0 008.354-5.646z" /></svg>
                  </button>
                }
                

                {/* Profile dropdown */}
                <div className="ml-3 relative">
                  <div>
                    <button className="max-w-xs dark:bg-black bg-white flex items-center text-sm rounded-full focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500" id="user-menu" aria-haspopup="true" onClick={toggleShowProfileDropdown}>
                      <span className="sr-only">Open user menu</span>
                      {
                        profileImageUrl && 
                        <img className="h-8 w-8 rounded-full" src={profileImageUrl} alt="profile image" />
                      }
                    </button>
                  </div>
                  {/*
                    Profile dropdown panel, show/hide based on dropdown state.

                    Entering: "transition ease-out duration-100"
                      From: "transform opacity-0 scale-95"
                      To: "transform opacity-100 scale-100"
                    Leaving: "transition ease-in duration-75"
                      From: "transform opacity-100 scale-100"
                      To: "transform opacity-0 scale-95"
                  */}
                  <Transition
                    show={showProfileDropdown}
                    enter="transition ease-out duration-100"
                    enterFrom="transform opacity-0 scale-95"
                    enterTo="transform opacity-100 scale-100"
                    leave="transition ease-in duration-75"
                    leaveFrom="transform opacity-100 scale-100"
                    leaveTo="transform opacity-0 scale-95">
                    <div className="origin-top-right absolute right-0 mt-2 w-48 rounded-md shadow-lg py-1 dark:bg-gray-900 bg-white ring-1 ring-black ring-opacity-5" role="menu" aria-orientation="vertical" aria-labelledby="user-menu">
                      <a href="#" onClick={signOut} className="block px-4 py-2 text-sm text-gray-700 dark:text-white dark:hover:bg-gray-800 hover:bg-gray-100" role="menuitem">Sign out</a>
                    </div>
                  </Transition>
                </div>
              </div>
            </div>
          </div>

            <main className="flex-1 relative overflow-y-auto focus:outline-none" tabIndex="0">
              <div className="py-6">
                <div className="max-w-7xl mx-auto px-4 sm:px-6 md:px-8">
                  {/* Replace with your content */}
                  {/* <div className="py-4">
                    <div className="border-4 border-dashed border-gray-200 rounded-lg h-96"></div>
                  </div> */}
                  {children}
                  {/* /End replace */}
                </div>
              </div>
            </main>
          </div>
        </>
      }
    </div>
      
  )
}

export default DefaultLayout