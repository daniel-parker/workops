import { useEffect, useState } from "react"
import { format } from "date-fns"

export const useTimestamp = (epochSeconds) => {
  const [humanReadable, setHumanReadable] = useState()
  
  useEffect(() => {
    if (epochSeconds) {
      const d = new Date(epochSeconds * 1000)

      setHumanReadable(format(d, "yyyy/MM/dd HH:mm:ss.SSS"))
    } else {
      setHumanReadable('')
    }
  }, [epochSeconds])
  
  return humanReadable
}