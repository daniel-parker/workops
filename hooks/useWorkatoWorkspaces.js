import { useSession } from "next-auth/client"
import { useEffect, useState } from "react"
import useLocalStorage from "./useLocalStorage"

const useWorkatoWorkspaces = () => {
  const [session] = useSession()
  const [workatoWorkspaces, setWorkatoWorkspaces] = useState(null)
  const [currentWorkspace, setCurrentWorkspace] = useLocalStorage('workspace')

  useEffect(() => {
    if (session) {
      (async () => {
        const res = await fetch('/api/workato/workspaces')
        const data = await res.json()
  
        if (data) {
          setWorkatoWorkspaces(data)
        }
      })()
    }
  }, [session])

  useEffect(() => {
    if (workatoWorkspaces && !currentWorkspace) {
      setCurrentWorkspace(Object.keys(workatoWorkspaces)[0])
    }
  }, [workatoWorkspaces])

  return workatoWorkspaces
}

export default useWorkatoWorkspaces
