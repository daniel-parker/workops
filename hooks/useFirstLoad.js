import { useEffect, useState } from "react"

export const useFirstLoad = () => {
  const [firstLoad, setFirstLoad] = useState(true)

  useEffect(() => {
    setFirstLoad(false)
  }, [])

  return firstLoad
}
