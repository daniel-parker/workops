export const constructFilterString = (recipeIds = [], recipeIdLabel = "recipeId") => {
  const sortedIds = recipeIds.sort()
  let filter = ''

  sortedIds.map(recipeId => {
    if (filter.length > 0) {
      filter = `${filter} OR`
    }

    filter = `${filter} labels.${recipeIdLabel}="${recipeId}"`
  })

  return filter
}
