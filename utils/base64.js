export const decodeBase64 = (b64String) => {
  const buffer = Buffer.from(b64String, 'base64')
  return buffer.toString('utf-8')
}