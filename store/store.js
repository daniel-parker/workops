import { configureStore } from '@reduxjs/toolkit'
import { createWrapper } from 'next-redux-wrapper'
import rootReducer from './slices/rootReducer'

const makeStore = context => configureStore({
  reducer: rootReducer,
})

export const wrapper = createWrapper(makeStore, {debug: true})
