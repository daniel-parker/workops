import { createSlice } from "@reduxjs/toolkit"

const logs = createSlice({
  name: 'logs',
  initialState: {
    nextRequest: null,
    entries: [],
  },
  reducers: {
    resetLogs(state) {
      state.nextRequest = null
      state.entries = []
    },
    addLogs(state, action) {
      if (Array.isArray(action.payload)) {
        const temp = [...state.entries, ...action.payload]
        // Filter out duplicates
        state.entries = temp.filter((l, index, self) => index === self.findIndex(t => t.insertId === l.insertId))
      }
    },
    setNextRequest(state, action) {
      if (action.payload === '') {
        state.nextRequest = null
      } else {
        state.nextRequest = action.payload
      }
    }
  }
})

export const {
  resetLogs,
  addLogs,
  setNextRequest,
} = logs.actions

export const loadLogs = (recipeIds) => async (dispatch, getState) => {
  const { nextRequest } = getState().logs

  if (recipeIds.length === 0) {
    return
  }

  const body = {
    recipeIds,
    nextRequest
  }

  const response = await fetch('/api/logs/loadLogs', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(body)
  })

  const data = await response.json()

  dispatch(addLogs(data[0]))
  if (data[1]) {
    dispatch(setNextRequest(data[1]))
  } else {
    dispatch(setNextRequest(null))
  }
}

export default logs.reducer
