import { combineReducers } from '@reduxjs/toolkit'
import { HYDRATE } from 'next-redux-wrapper'

import integrations from './integrations'
import logs from './logs'

// This helps next.js hydrate state correctly
const hydrateReducer = (state = {}, action) => {
  switch (action.type) {
    case HYDRATE:
      return { ...state, ...action.payload }
    default:
      return state
  }
}

export default combineReducers({
  hydrateReducer,
  integrations,
  logs,
})
