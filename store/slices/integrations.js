import { createSlice } from "@reduxjs/toolkit"
import bb from 'bluebird'
import { fetchRecipesForIntegration, getIntegrationStatusInformation, getRecipeDatas } from "../../services/recipesService"

const integrations = createSlice({
  name: 'integrations',
  initialState: {
    list: [],
    statuses: {},
    loading: false,
  },
  reducers: {
    setLoading(state, action) {
      state.loading = action.payload
    },
    setIntegrations(state, action) {
      state.list = action.payload
    },
    setIntegrationStatuses(state, action) {
      state.statuses = action.payload
    }
  }
})

export const {
  setLoading,
  setIntegrations,
  setIntegrationStatuses,
} = integrations.actions

export const loadIntegrationStatuses = (currentWorkspace) => async (dispatch, getState) => {
  let statuses = null

  const { list } = getState().integrations

  dispatch(setLoading(true))
  await bb.each(list, async integration => {
    const [triggerRecipes, recipes] = await fetchRecipesForIntegration(integration, currentWorkspace)
    const recipeDatas = await getRecipeDatas(recipes, currentWorkspace)
    const [integrationStatus] = getIntegrationStatusInformation(triggerRecipes, recipeDatas)
    statuses = {
      ...statuses,
      [integration.name]: integrationStatus
    }
  })

  dispatch(setIntegrationStatuses(statuses))
  dispatch(setLoading(false))
}

export default integrations.reducer
