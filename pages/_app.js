import "tailwindcss/tailwind.css";
import { Provider } from 'next-auth/client'
import { wrapper } from "../store/store"

const MyApp = ({ Component, pageProps }) => {
  return (
    <Provider session={pageProps.session}>
      <Component {...pageProps} />
    </Provider>
  )
}

export default wrapper.withRedux(MyApp)
