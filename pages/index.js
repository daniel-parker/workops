import { useEffect, useState } from 'react'
import DefaultLayout from '../layouts/DefaultLayout'
import {ClipLoader} from 'react-spinners'
import classNames from 'classnames'
import { IntegrationStatus } from '../services/recipesService'
import useLocalStorage from '../hooks/useLocalStorage'
import workatoIntegrations from './integrations/integrations.json'
import { useDispatch, useSelector } from 'react-redux'
import { loadIntegrationStatuses, setIntegrations } from '../store/slices/integrations'

const Integration = ({ className, integration, status }) => {
  const [loadingIntegration, setLoadingIntegration] = useState(false)

  const setLoading = () => {
    setLoadingIntegration(true)
    setTimeout(() => {
      window.location.pathname = `/integrations/${encodeURIComponent(integration.name)}`
    }, 0)
  }

  return (
    <div className={classNames(className, 'rounded border dark:border-gray-800 border-gray-400 dark:bg-gray-900 bg-white relative')}>
      {
        loadingIntegration &&
        <div className="fixed h-full w-full top-0 left-0 z-50 bg-gray-500 bg-opacity-50 flex flex-col items-center justify-center">
          <span className="text-4xl text-white font-semibold mb-4">Loading Integration Dashboard</span>
          <ClipLoader color="#fff" size="150"></ClipLoader>
        </div>
      }
      {
        status === IntegrationStatus.ON &&
        <div className="absolute w-full h-2 top-0 bg-green-500 rounded-t-sm"></div>
      }
      {
        status === IntegrationStatus.OFF &&
        <div className="absolute w-full h-2 top-0 bg-gray-500 rounded-t-sm"></div>
      }
      {
        status === IntegrationStatus.PARTIAL &&
        <div className="absolute w-full h-2 top-0 bg-yellow-400 rounded-t-sm"></div>
      }
      {
        status === IntegrationStatus.ERROR &&
        <div className="absolute w-full h-2 top-0 bg-red-500 rounded-t-sm"></div>
      }
      <div className="flex flex-col p-4 h-full">
        <div className="flex flex-row items-center">
          <div className="flex flex-col">
            <span className="dark:text-gray-200 text-gray-700 font-semibold mb-2">{integration.name}</span>
            {
              integration.description &&
              <span className="text-gray-400 text-sm">{integration.description}</span>
            }
          </div>
          <div className="flex flex-row items-center ml-4">
            <a onClick={setLoading} className="cursor-pointer inline-flex items-center px-2.5 py-1.5 border border-transparent text-xs font-medium rounded text-indigo-700 bg-indigo-100 hover:bg-indigo-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500" title="Open Integration Dashboard">
              Dashboard
            </a>
          </div>
        </div>
      </div>
    </div>
  )
}

const Integrations = () => {
  const loading = useSelector(state => state.integrations.loading)
  const integrationStatuses = useSelector(state => state.integrations.statuses)
  const [currentWorkspace] = useLocalStorage('workspace')
  const integrations = useSelector(state => state.integrations.list)

  const dispatch = useDispatch()

  useEffect(() => {
    if (currentWorkspace) {
      dispatch(setIntegrations(workatoIntegrations[currentWorkspace].integrations))
    }
  }, [currentWorkspace])

  useEffect(() => {
    dispatch(loadIntegrationStatuses(currentWorkspace))
  }, [integrations, currentWorkspace])

  return (
    <DefaultLayout>
      <h1 className="text-2xl dark:text-white text-gray-600 mb-4 font-bold">Integrations</h1>
      {
        loading &&
        <span className="dark:text-white text-gray-900">Refreshing integration states...</span>
      }
      <div className="grid grid-cols-1 md:grid-cols-3 gap-4">
        {
          integrations && integrations.map(integration => (
            <Integration key={integration.name} integration={integration} status={integrationStatuses[integration.name]}/>
          ))
        }
      </div>
    </DefaultLayout>
  )
}

export default Integrations
