import { getSession } from "next-auth/client"
import logsService from "../../../services/logsService"

export default async (req, res) => {
  const session = await getSession({ req })

  if (!session) {
    res.statusCode = 401
    res.end()
    return
  }

  if (req.method === 'POST') {
    try {
      const response = await logsService.loadLogs(req.body.recipeIds, req.body.nextRequest)

      res.statusCode = 200
      res.json(response)
    } catch (err) {
      res.statusCode = 500
      res.json(err)
      return
    }
  } else {
    res.statusCode = 400
    res.end()
    return
  }
}
