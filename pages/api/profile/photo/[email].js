import md5 from "md5"

export default (req, res) => {
  if (req.method === 'GET') {
    const email = req.query.email.replace(' ', '')

    const hash = md5(email.toLowerCase())

    res.statusCode = 200
    res.json({
      url: `https://www.gravatar.com/avatar/${hash}`
    })
  } else {
    res.statusCode = 400
    res.end()
  }
}