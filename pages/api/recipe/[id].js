import { getSession } from "next-auth/client"

export default async (req, res) => {
  const session = await getSession({ req })

  if (!session) {
    res.statusCode = 401
    res.end()
    return
  }

  if (req.method === 'GET') {
    if (!req.headers['x-workspace']) {
      res.statusCode = 400
      return
    }

    try {
      const result = await fetch(`https://www.workato.com/api/recipes/${req.query.id}`, {
        method: 'GET',
        headers: {
          'x-user-email': JSON.parse(process.env.WORKATO_WORKSPACES)[req.headers['x-workspace']].email,
          'x-user-token': JSON.parse(process.env.WORKATO_WORKSPACES)[req.headers['x-workspace']].token,
        }
      })

      const data = await result.json()

      res.statusCode = 200
      res.json(data)
      return

    } catch (err) {
      res.statusCode = 500
      res.json(err)
      return
    }
  } else {
    res.statusCode = 400
    res.end()
    return
  }
}
