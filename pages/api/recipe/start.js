import { getSession } from "next-auth/client"

export default async (req, res) => {
  const session = await getSession({ req })

  if (!session) {
    res.statusCode = 401
    res.end()
    return
  }


  if (req.method === 'PUT') {
    if (!req.headers['x-workspace']) {
      res.statusCode = 400
      return
    }

    try {
      const response = await fetch(`https://www.workato.com/api/recipes/${req.body.id}/start`, {
        method: 'PUT',
        headers: {
          'x-user-email': JSON.parse(process.env.WORKATO_WORKSPACES)[req.headers['x-workspace']].email,
          'x-user-token': JSON.parse(process.env.WORKATO_WORKSPACES)[req.headers['x-workspace']].token,
        }
      })

      const data = await response.json()

      res.statusCode = 200
      res.json(data)
    } catch (err) {
      res.statusCode = 500
      res.json(err)
      return
    }
  } else {
    res.statusCode = 400
    res.end()
    return
  }
}
