import { getSession } from 'next-auth/client'

export default async (req, res) => {
  if (req.method === 'GET') {
    const session = await getSession({ req })
    if (session) {
      const workatoWorkspaces = JSON.parse(process.env.WORKATO_WORKSPACES)
      res.statusCode = 200
      res.json(workatoWorkspaces)
    } else {
      res.statusCode = 401
      res.end()
    }
  } else {
    res.statusCode = 404
    res.end()
  }
}
