import { useEffect, useState } from 'react'
import Recipe from '../../components/RecipeComponent'
import DefaultLayout from '../../layouts/DefaultLayout'
import { fetchRecipesForIntegration, IntegrationStatus, getIntegrationStatusInformation, getRecipeDatas } from '../../services/recipesService'
import classNames from 'classnames'
import workatoIntegrations from './integrations.json'
import useLocalStorage from '../../hooks/useLocalStorage'
import { useFirstLoad } from '../../hooks/useFirstLoad'
import { loadLogs, resetLogs } from '../../store/slices/logs'
import { useDispatch, useSelector } from 'react-redux'
import { setIntegrations } from '../../store/slices/integrations'
import { LogsListComponent } from '../../components/logs/LogsListComponent'
import { constructFilterString } from '../../utils/gcpFilterString'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faExternalLink, faHeartbeat } from '@fortawesome/pro-duotone-svg-icons'

const Integration = ({ integrationName }) => {
  const [loading, setLoading] = useState(true)
  const firstLoad = useFirstLoad()
  const [triggerRecipes, setTriggerRecipes] = useState([])
  const [recipes, setRecipes] = useState([])
  const [integrationStatus, setIntegrationStatus] = useState(IntegrationStatus.OFF)
  const [localTriggerRecipes, setLocalTriggerRecipes] = useState(triggerRecipes)
  const [allRecipes, setAllRecipes] = useState([])
  const [currentWorkspace] = useLocalStorage('workspace')

  const logEntries = useSelector(state => state.logs.entries)
  const nextRequest = useSelector(state => state.logs.nextRequest)
  const [gcpLogsLink, setGcpLogsLink] = useState()

  const dispatch = useDispatch();

  useEffect(() => {
    setLoading(true)
    if (currentWorkspace) {
      dispatch(setIntegrations(workatoIntegrations[currentWorkspace].integrations))
    }
  }, [currentWorkspace])

  useEffect(() => {
    if (Array.isArray(allRecipes)) {
      const filter = constructFilterString(allRecipes.map(r => r.id))

      const queryParam = encodeURIComponent(filter)
      setGcpLogsLink(`https://console.cloud.google.com/logs/query;query=${queryParam}`)
    }
  }, [allRecipes])

  useEffect(() => {
    const combinedRecipes = [
      ...triggerRecipes,
      ...recipes,
    ]

    setAllRecipes(combinedRecipes)
  }, [triggerRecipes, recipes])

  useEffect(() => {
    (async () => {
      const integrationIndex = workatoIntegrations[currentWorkspace].integrations.findIndex(integ => integ.name === integrationName)

      const integration = workatoIntegrations[currentWorkspace].integrations[integrationIndex]

      const [triggerRecipes, dependsOnRecipes] = await fetchRecipesForIntegration(integration, currentWorkspace)

      const recipeDatas = await getRecipeDatas(dependsOnRecipes, currentWorkspace)

      setRecipes(recipeDatas)
      setTriggerRecipes(triggerRecipes)
      setLoading(false)
    })()
  }, [currentWorkspace])

  useEffect(() => {
    const [lIntegrationStatus, triggerRecipesWithErrorInformation] = getIntegrationStatusInformation(triggerRecipes, recipes)

    setLocalTriggerRecipes(triggerRecipesWithErrorInformation)
    setIntegrationStatus(lIntegrationStatus)
  }, [triggerRecipes, recipes])

  useEffect(() => {
    if (!firstLoad) {
      dispatch(resetLogs())
      dispatch(loadLogs(allRecipes.map(r => r.id)))
    }
  }, [allRecipes, firstLoad])

  const loadMoreLogs = () => {
    dispatch(loadLogs(allRecipes.map(r => r.id)))
  }

  const getIntegrationStatusTextClassNames = (integrationStatus) => {
    switch (integrationStatus) {
      case IntegrationStatus.ON:
        return 'text-green-600'
      case IntegrationStatus.PARTIAL:
        return 'text-yellow-400'
      case IntegrationStatus.ERROR:
        return 'text-red-600'
      default:
        return 'text-gray-700'
    }
  }

  const getIntegrationStatusBackgroundClassNames = (integrationStatus) => {
    switch (integrationStatus) {
      case IntegrationStatus.ON:
        return 'bg-green-600'
      case IntegrationStatus.PARTIAL:
        return 'bg-yellow-400'
      case IntegrationStatus.ERROR:
        return 'bg-red-600'
      default:
        return 'bg-gray-700'
    }
  }

  return (
    <DefaultLayout>
      <div className="flex flex-row justify-between">
        <div className="w-1/2">
          <span className="md:text-lg dark:text-gray-300 text-gray-500">Integration Name</span>
          <h1 className="font-bold text-lg md:text-2xl dark:text-white text-gray-600 mb-4">{integrationName}</h1>
          {
            loading && <span className="dark:text-white text-gray-600">Loading integration details...</span>
          }
        </div>
        <div className="flex flex-col items-end w-1/2">
          <span className="md:text-lg dark:text-gray-200 text-gray-500 text-right w-full">Integration Status <FontAwesomeIcon icon={faHeartbeat}/></span>
          <div className="flex flex-row items-center">
            <div className={classNames(getIntegrationStatusBackgroundClassNames(integrationStatus), 'h-4 w-4 rounded-full hidden md:block mr-2')}></div>
            <h2 className={classNames(getIntegrationStatusTextClassNames(integrationStatus), 'font-semibold text-xl text-right md:text-left')}>{integrationStatus}</h2>
          </div>
        </div>
      </div>
      
      <div>
        <h2 className="text-lg dark:text-gray-200 text-gray-500 font-semibold mb-2 mt-4"><span className="uppercase">Trigger Recipes</span><br/><span className="text-sm font-base text-gray-400 italic">The root "trigger" recipes used in this integration</span></h2>
        <div className="grid grid-cols-1 gap-4 md:grid-cols-3">
          {localTriggerRecipes.map(recipe => (
            <Recipe recipe={recipe} key={recipe.id} canStop={true} />
          ))}
        </div>

        <h2 className="text-lg dark:text-gray-200 text-gray-500 font-semibold mb-2 mt-4"><span className="uppercase">Used Recipes</span><br/><span className="text-sm font-base text-gray-400 italic">Recipes called from the trigger recipes</span></h2>
        <div className="grid grid-cols-1 md:grid-cols-3 gap-4">
          {recipes.map(recipe => (
            <Recipe recipe={recipe} key={recipe.id} />
          ))}
        </div>
      </div>

      <div className="flex flex-row justify-between items-center">
        <h2 className="text-lg dark:text-gray-200 text-gray-500 font-semibold mb-2 mt-4"><span className="uppercase">Integration Logs</span><br/><span className="text-sm font-base text-gray-400 italic">Recent GCP logs for this integration</span></h2>
        <div>
          <a href={gcpLogsLink} target="_blank" className="text-blue-500">Open in GCP Logging <FontAwesomeIcon icon={faExternalLink} /></a>
        </div>
      </div>
      
      <div className="mt-4 overflow-y-auto w-full" style={{ height: '500px' }}>
        <LogsListComponent className="w-full" logEntries={logEntries} recipes={allRecipes}></LogsListComponent>
      </div>

      {
        nextRequest && <button onClick={loadMoreLogs}>Load more logs</button>
      }
    </DefaultLayout>
  )
}
  
export async function getServerSideProps({ params }) {
  return {
    props: {
      integrationName: decodeURIComponent(params.name),
    }
  }
}

export default Integration