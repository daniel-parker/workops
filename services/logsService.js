
import config from '../workops.config.json'

import GCPLogsService from './logSources/gcpLogsService'

const LOG_SOURCES = {
  GCP: 'gcp'
}

let logService

switch (config.logSource) {
  case LOG_SOURCES.GCP:
    logService = new GCPLogsService(config.logSourceOptions)
    break;
  default:
    break;
}

const loadLogs = logService.loadLogs

export default {
  loadLogs,
}