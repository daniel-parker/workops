import bb from 'bluebird'
import { sumBy, uniq } from 'lodash'

const IntegrationStatus = {
  OFF: 'Off',
  ON: 'Fully Operational',
  PARTIAL: 'Partially Operational',
  ERROR: 'Integration is in a non-operational error state'
}

const traverseBlock = (block) => {
  return block.reduce((recipes, step) => {
      if (step.name && (step.name === 'call_service' || step.name === 'call_service_async')) {
          recipes.push(step.input.flow_id)
      }
      
      if (step.block) {
          const recipesBelow = traverseBlock(step.block)
          recipes = [
              ...recipes,
              ...recipesBelow
          ]
      }
      
      return recipes
  }, [])
}

const fetchRecipesForIntegration = async (integration, workspace) => {
  const triggerRecipes = []

  const recipes = await bb.reduce(integration.triggerRecipes, async (recipes, recipe) => {
    const res = await fetch(`/api/recipe/${recipe}`, {
      method: 'GET',
      headers: {
        'x-workspace': workspace
      }
    })

    const data = await res.json()

    triggerRecipes.push(data)

    return [
      ...recipes,
      ...traverseBlock(JSON.parse(data.code).block)
    ]
  }, [])

  return [triggerRecipes, uniq(recipes)]
}

const getRecipeDatas = async (recipeIds, workspace) => {
  const recipeDatas = await bb.map(recipeIds, async (recipe) => {
    const res = await fetch(`/api/recipe/${recipe}`, {
      method: 'GET',
      headers: {
        'x-workspace': workspace
      }
    })
    
    const data = await res.json()

    return data
  })

  return recipeDatas
}

const getIntegrationStatusInformation = (triggerRecipes, recipes) => {
  let integrationStatus = IntegrationStatus.OFF

  const triggerRecipeStatuses = triggerRecipes.reduce((accum, recipe) => {
    accum.push(recipe.running)
    return accum
  }, [])

  const totalOn = sumBy(triggerRecipeStatuses, s => s ? 1:0)

  if (totalOn == 0) {
    integrationStatus = IntegrationStatus.OFF
  } else if (totalOn < triggerRecipeStatuses.length) {
    integrationStatus = IntegrationStatus.PARTIAL
  } else {
    integrationStatus = IntegrationStatus.ON
  }

  const recipeStatusMap = recipes.reduce((recipeStatusMap, recipe) => {
    recipeStatusMap[recipe.id] = recipe.running
    return recipeStatusMap
  }, {});

  const triggerRecipesWithErrorInformation = triggerRecipes.map(tr => {
    const ids = traverseBlock(JSON.parse(tr.code).block)
    
    const copyTr = JSON.parse(JSON.stringify(tr))

    for (let i = 0; i < ids.length; i++) {
      if (!recipeStatusMap[ids[i]]) {
        copyTr.errorState = true
        integrationStatus = IntegrationStatus.ERROR
        break
      }
    }

    return copyTr
  })

  return [integrationStatus, triggerRecipesWithErrorInformation]
}

export {
  IntegrationStatus,
  traverseBlock,
  fetchRecipesForIntegration,
  getRecipeDatas,
  getIntegrationStatusInformation,
}