import { Logging } from '@google-cloud/logging'
import { decodeBase64 } from '../../utils/base64'
import { constructFilterString } from '../../utils/gcpFilterString'

export default class GCPLogsService {
  constructor(options) {
    this.logging = new Logging({
      projectId: options.gcpProject,
      credentials: JSON.parse(decodeBase64(process.env.GCP_CREDENTIALS))
    })

    this.recipeIdLabel = options.recipeIdLabel

    this.loadLogs = this.loadLogs.bind(this)
  }

  async loadLogs(recipeIds = [], nextRequest = null) {
    if (nextRequest) {
      return await this.logging.getEntries(nextRequest)
    }

    const filter = constructFilterString(recipeIds, this.recipeIdLabel)

    const result = await this.logging.getEntries({
      filter: filter,
      pageSize: 100,
      orderBy: 'timestamp desc',
    })

    return result
  }
}
